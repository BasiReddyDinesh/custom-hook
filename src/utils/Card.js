import React from "react";
import PropTypes from "prop-types";
import { useHistory } from "react-router-dom";

const Card = (props) => {
  const history = useHistory();
  const usersDetail = (id) => {
    history.push(`/user/${id}`);
  };

  return (
    <div className="container" data-test="card-component">
      {props.list?.map((item) => (
        <section className="card" key={item.id}>
          <div className="item">{item.id}</div>
          <div className="item">{item.name}</div>
          <div className="item">
            <button className="btn" onClick={() => usersDetail(item.id)}>
              click for more info...
            </button>
          </div>
        </section>
      ))}
    </div>
  );
};

Card.propTypes = { list: PropTypes.array.isRequired };

export default Card;
