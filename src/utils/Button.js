import React from "react";
import PropTypes from "prop-types";

function Button({ value, clickHandler }) {
  return (
    <span data-test="Button-component">
      <input
        data-test={`counter-button-${value}`}
        className="buttons"
        type={"button"}
        value={value}
        onClick={clickHandler}
      />
    </span>
  );
}

Button.propTypes = {
  value: PropTypes.string.isRequired,
  clickHandler: PropTypes.func.isRequired,
};

export default Button;
