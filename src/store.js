import { legacy_createStore as createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";

import rootReducers from "./rootReducers";

// const reduxThunk = applyMiddleware(thunk);

export default createStore(rootReducers, {}, applyMiddleware(thunk));
