import React from "react";
import { useParams } from "react-router-dom";
import PropTypes from "prop-types";

import { connect } from "react-redux";

let userList = undefined;

function UserDetails(props) {
  const [user, setUser] = React.useState(undefined);
  const { id } = useParams();
  React.useEffect(() => {
    userList = userList || props.user[Number(id) - 1];
    setUser(userList);
  }, []);

  console.log(user);

  return (
    <div data-test="userDetails-component">
      <div className="user_container">
        <div className="item">{user?.id}</div>
        <div className="item">{user?.name}</div>
        <div className="item">{user?.email}</div>
        <div className="item">{user?.company.name}</div>
        <div className="item">{user?.phone}</div>
        <div className="item">{user?.username}</div>
        <div className="item">{user?.website}</div>
      </div>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    user: state.users,
  };
};

UserDetails.propTypes = {};

export default connect(mapStateToProps)(UserDetails);
