import React from "react";
import Card from "../utils/Card";
import { useDispatch, useSelector } from "react-redux";
import useSkipfirstrender from "../customHook/useSkipfirstrender";

import { fetchUsers } from "../actions";

function UserAccounts(props) {
  const [reload, setReload] = React.useState(false);
  const [count, setCount] = React.useState(0);
  const dispatch = useDispatch();
  const users = useSelector((store) => store.users);

  const descrememt = () => {
    setCount(count - 1);
  };
  const increment = () => {
    setCount(count + 1);
  };

  const refreshPage = () => {
    setReload((reload) => (reload ? reload : !reload));
  };
  useSkipfirstrender(() => {
    dispatch(fetchUsers());
  }, [reload, count, users]);

  return (
    <div data-test="userAccount-component">
      <>
        <h3>{count}</h3>
        <div>
          <button className="buttons" onClick={increment}>
            Increment
          </button>
          <button className="buttons" onClick={descrememt}>
            Descrememt
          </button>
        </div>
      </>
      {users.length > 0 ? (
        <Card list={users} />
      ) : (
        <>
          <h3 className="warning">
            warning:user info will not be shown on the page on initial render
            until you perform an action. please perform an action on the page to
            load the user info.
          </h3>
          <button className="buttons" onClick={refreshPage}>
            reload
          </button>
        </>
      )}
    </div>
  );
}

export default React.memo(UserAccounts);
