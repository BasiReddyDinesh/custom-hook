import React from "react";
import { mount } from "enzyme";
import { store } from "../store";
import { Provider } from "react-redux";
import { findElementByDataTest } from "../testUtils";

import Counter from "./Counter";

const setup = () => {
  return mount(
    <Provider store={store}>
      <Counter />
    </Provider>
  );
};

describe("render Counter component without fail and intial state", () => {
  let wrapper;
  beforeEach(() => {
    wrapper = setup();
  });
  it("render Counter component without fail", () => {
    const counterComp = findElementByDataTest(wrapper, "counter-component");
    expect(counterComp.exists()).toBeTruthy();
  });
  it("counter initial state should be '0'", () => {
    const counterState = findElementByDataTest(wrapper, "counter-value");
    expect(counterState.text()).toBe("0");
  });
});
