import { combineReducers } from "redux";
import { counterReducer } from "./reducers/index";

export const reducers = combineReducers({
  counter: counterReducer,
});
