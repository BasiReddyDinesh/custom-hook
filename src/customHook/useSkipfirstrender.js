let flag = false;
let arr = [];
function useSkipfirstrender(func, state) {
  if (!flag) {
    flag = true;
    console.log(state);
    arr = [...state];
    return;
  }
  if (JSON.stringify(arr) !== JSON.stringify(state)) {
    console.log("componet-re-rendered");
    arr = [...state];
    func();
  }
}

export default useSkipfirstrender;
